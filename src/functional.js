
function compose2(f, g) {
  return (x) => g(f(x));
}

//compose n functions
function compose(functions) {
  return functions.reduce(compose2);
}

function run(initial_state, functions) {
  let composed_function = compose(functions);
  return composed_function(initial_state);
}

function safe(initial_state, functions) {
  try {
    return run(initial_state, functions);
  } catch (err) {
    return JSON.stringify(err);
  }
}

module.exports = {
  compose:compose,
  run:run,
  safe:safe
};
