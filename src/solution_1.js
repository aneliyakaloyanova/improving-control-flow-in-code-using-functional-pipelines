let {notify} = require('./commons');


let repository = {
  "200": {
    "offer-id": 200,
    "percentage": 15,
    "active": true,
    "requests": 0
  },
  "400": {
    "offer-id": 400,
    "percentage": 22,
    "active": false,
    "requests": 0
  }
};



function getOfferById(id) {
  var response = {};
  if (typeof id !== 'string')
    response = {
      "code": 400,
      "body": {
        "errors": ["The id you provided is invalid"]
      }
    };
  else if (repository[id]) {
    if (repository[id].active) {
      let offer = repository[id];
      offer.requests++;
      response = {
        code: 200,
        body: offer
      };
    } else {
      notify("Offer expired: " + id);
      response = {
        code: 400,
        body: {
          errors: ["The offer expired"]
        }
      };
    }
  } else {
    notify("Offer not found: " + id);
    response = {
      code: 404,
      body: {
        errors: ["The id you provided cannot be found"]
      }
    };
  }
  return JSON.stringify(response);
}


module.exports = {
  getOfferById:getOfferById
};




