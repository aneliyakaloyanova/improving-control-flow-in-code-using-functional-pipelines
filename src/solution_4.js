let {notify} = require('./commons');

let repository = {
  "200": {
    "offer-id": 200,
    "percentage": 15,
    "active": true,
    "requests": 0
  },
  "400": {
    "offer-id": 400,
    "percentage": 22,
    "active": false,
    "requests": 0
  }
};



class IResponse {
  constructor(data) {
    this.data = data;
  }
  then(fn) {
    //todo
  }
  fail(fn) {
    //todo
  }
  response() {
    return this.data;
  }
}
//do
class SuccesfullResponse extends IResponse {
  then(fn) {
    return fn(this);
  }
  fail(fn) {
    return this;
  }
}
//do not
class ErrorResponse extends IResponse {
  then(fn) {
    return this;
  }
  fail(fn) {
    return fn(this);
  }
}

function validateId(id) {
  if (typeof id !== 'string') {
    return new ErrorResponse({
      "code": 400,
      "body": {
        "errors": ["The id you provided is invalid"]
      }
    });
  }
  return new SuccesfullResponse({
    id: id
  });
}

function findOfferById(state) {
  if (!repository[state.data.id]) {
    notify("Offer not found: " + state.data.id);
    return new ErrorResponse({
      "code": 404,
      "body": {
        "errors": ["The id you provided cannot be found"]
      }
    });
  }
  return new SuccesfullResponse(state.data);
}

function validateOfferNotExpired(state){
  if (!repository[state.data.id].active) {
    notify("Offer expired: " + state.data.id);
    return new ErrorResponse({
        "code": 400,
        "body": {
          "errors": ["The offer expired"]
        }
    });
  }
  return new SuccesfullResponse(state.data);
}

function updateOffer(state) {
  let offer = repository[state.data.id];
  offer.requests++;
  return new SuccesfullResponse({
    "code": 200,
    "body": offer
  });
}



function json(state) {
  return new SuccesfullResponse(JSON.stringify(state.data));
}


function getOfferById(requestId) {
  return validateId(requestId)
  .then(findOfferById)
  .then(validateOfferNotExpired)
  .then(updateOffer)
  .then(json)
  .fail(json)
  .response();
}




module.exports = {
  getOfferById:getOfferById
};




